﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Drawing.Imaging;

namespace EMedia
{
    public partial class Form1 : Form
    {
        

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            using (var imageStream = File.OpenRead("Lena.jpg"))
            {
                var decoder = BitmapDecoder.Create(imageStream, BitmapCreateOptions.IgnoreColorProfile,
                    BitmapCacheOption.Default);
                var height = decoder.Frames[0].PixelHeight;
                var width = decoder.Frames[0].PixelWidth;
                MessageBox.Show("h:" + height + " p: " + width);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var fileLength = new FileInfo("Lena.jpg").Length;
            MessageBox.Show("s: " + fileLength);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (var imageStream = File.OpenRead("Lena.jpg"))
            {
                var decoder = BitmapDecoder.Create(imageStream, BitmapCreateOptions.IgnoreColorProfile,
                    BitmapCacheOption.Default);
                var depthofcolors = decoder.Frames[0].Format.BitsPerPixel;
                MessageBox.Show("color depth: " + depthofcolors);
            }
        }

    
        private void button4_Click_1(object sender, EventArgs e)
        {
           
        }
    }
}
